from django.contrib import admin
from django.urls import path
from .views import (
  Login,
  Logout,
  Register,
  FileUpload,
  UploadHistory,
  ProfileUser,
  PredictFood
)


urlpatterns = [
  path('login/', Login.as_view(), name='login'),
  path('logout/', Logout.as_view(), name='logout'),
  path('register/', Register.as_view(), name='register'),
  path('upload/food/', FileUpload.as_view(), name='upload_food'),
  path('user/history/', UploadHistory.as_view(), name='upload_history'),
  path('user/profile/', ProfileUser.as_view(), name='user_profile'),
  path('user/history/<int:id>', UploadHistory.as_view(), name='single_food_history'),
  path('user/predict_food/' ,PredictFood.as_view(), name='predict_food')
]