from django.db import models
from django.contrib.auth.models import User
import hashlib, os, re
from uuid import uuid4

def get_upload_path(instance, filename):
  filename = re.sub('[^0-9a-zA-Z.]+', '_', filename)
  return "{}/{}_{}".format(uuid4().hex, uuid4().hex, filename)

class Foods(models.Model):
  user        = models.ForeignKey(User, on_delete=models.CASCADE)
  file        = models.ImageField(upload_to=get_upload_path)
  create_time = models.DateTimeField(auto_now=True)
