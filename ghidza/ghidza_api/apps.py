from django.apps import AppConfig


class GhidzaApiConfig(AppConfig):
    name = 'ghidza_api'
