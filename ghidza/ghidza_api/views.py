from django.contrib.auth import authenticate
from rest_framework.authtoken.models import Token
from rest_framework.views import APIView
from .serializers import UserSerializer, UploadSerializer, HistorySerializer
from .models import Foods
from django.contrib.auth.models import User
from rest_framework.decorators import parser_classes
from rest_framework.parsers import FileUploadParser
from rest_framework.response import Response
from ml.ml_ghidza import predict_food
from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK
)


class Login(APIView):
  permission_classes = ()

  def post(self, request):
    username = request.data.get("username")
    password = request.data.get("password")
    if username is None or password is None:
      return Response({'error': 'Please provide both username and password'}, status=HTTP_400_BAD_REQUEST)

    user = authenticate(username=username, password=password)
    if not user:
      return Response({'error': 'Invalid Credentials'}, status=HTTP_404_NOT_FOUND)

    token, _ = Token.objects.get_or_create(user=user)
    return Response({'Authentication Token': token.key}, status=HTTP_200_OK)


class Logout(APIView):
  def get(self, request):
      request.user.auth_token.delete()
      return Response({'success': 'Success Logout'}, status=HTTP_200_OK)


class Register(APIView):
  permission_classes  = ()

  def post(self, request):
    user_serializer = UserSerializer(data=request.data)
    if user_serializer.is_valid():
      user_serializer.save()
      return Response({'success': 'Success register new user'}, status=HTTP_200_OK)
    else:
      return Response(user_serializer.errors, status=HTTP_400_BAD_REQUEST)
    return Response(user_serializer.errors, status=HTTP_400_BAD_REQUEST)


class FileUpload(APIView):

  def post(self, request):
    image_file = UploadSerializer(data=request.data, context={'request': request})
    if image_file.is_valid():
      saved_image = image_file.save()
      return Response({'success': "Image upload success"}, status=204)
    else:
      return Response({'error': "Only accept valid image file"}, status=HTTP_400_BAD_REQUEST)


class UploadHistory(APIView):

  def get(self, request):
    liss = Foods.objects.filter(user_id=request.user.id)
    serializer = HistorySerializer(liss, many=True)
    return Response(serializer.data)


class ProfileUser(APIView):

  def get(self, request):
    user_profile = User.objects.get(pk=request.user.id)
    serializer = UserSerializer(user_profile)
    return Response(serializer.data)

  def patch(self, request):
    user_profile = User.objects.get(pk=request.user.id)
    serializer = UserSerializer(user_profile, data=request.data, partial=True)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data)
    else:
      return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)


class PredictFood(APIView):

  def post(self, request):
    image_file = UploadSerializer(data=request.data, context={'request': request})
    valid = image_file.is_valid()
    if valid:
      saved_image = image_file.save()
      saved_image = saved_image.file.url
      type_food = predict_food(saved_image)
      
      return Response({'Food Prediction Result': type_food}, status=200)
    else:
      return Response({'error': "Only accept valid image file"}, status=HTTP_400_BAD_REQUEST)







