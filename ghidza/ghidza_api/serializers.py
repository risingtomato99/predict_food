from django.contrib.auth.models import User
from .models import Foods
from rest_framework import serializers
from django.contrib.auth.hashers import make_password
from rest_framework.validators import UniqueValidator


class UserSerializer(serializers.Serializer):
  username = serializers.CharField(required=True, max_length=20, validators=[UniqueValidator(queryset=User.objects.all())])
  email = serializers.EmailField(required=True, validators=[UniqueValidator(queryset=User.objects.all())])
  first_name = serializers.CharField(max_length=30, required=False)
  last_name = serializers.CharField(max_length=30, required=False)
  password = serializers.CharField(write_only=True, required=True, min_length=6)


  def create(self, validated_data):
    validated_data['password'] = make_password(validated_data.get('password'))
    user = User.objects.create(**validated_data)
    return user

  def update(self, instance, validated_data):
    instance.first_name   = validated_data.get('first_name', instance.first_name)
    instance.last_name    = validated_data.get('last_name', instance.last_name)
    instance.username     = validated_data.get('username', instance.username)
    instance.save()
    return instance


class UploadSerializer(serializers.ModelSerializer):

  class Meta:
    model = Foods
    fields = ('file',)

  def create(self, validated_data):
    image = Foods(
      file=validated_data['file'],
    )

    image.user_id = self.context['request'].user.id
    image.save()
    return image


class HistorySerializer(serializers.Serializer):
  id          = serializers.IntegerField()    
  file        = serializers.CharField()
  create_time = serializers.CharField()

