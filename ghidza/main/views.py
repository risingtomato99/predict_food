from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic import TemplateView, View


# Create your views here.

class HomeView(TemplateView):
    template_name = "main/index.html"
    def get(self, request):
        return render(request, self.template_name)


