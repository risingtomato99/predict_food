import numpy as np
import time
import sys
import os
import cv2
import keras
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D, Dense, Flatten
from keras import backend as K

img_width, img_height = 200, 150

dict_food = {
	0: "Bakso",
	1: "Bubur Ayam",
	2: "Mie Ayam",
	3: "Nasi Ayam Geprek",
	4: "Nasi Goreng",
	5: "Nasi Telur",
	6: "Pizza",
	7: "Siomay"
}

def predict_food(filename):
	model_ml = get_model()
	image = load_image(os.getcwd() + filename)
	result = model_ml.predict(image)[0].tolist()
	print(result)
	type_food = dict_food[result.index(1)]
	K.clear_session()

	return type_food


def get_model():
	model = Sequential()
	model.add(Conv2D(64, kernel_size=(3,3), activation = 'relu', input_shape=(img_width, img_height, 3) ))
	model.add(MaxPooling2D(pool_size = (2, 2)))

	model.add(Conv2D(64, kernel_size = (3, 3), activation = 'relu'))
	model.add(MaxPooling2D(pool_size = (2, 2)))

	model.add(Flatten())
	model.add(Dense(120, activation = 'relu'))
	model.add(Dense(8, activation = 'softmax'))

	model.compile(loss = 'categorical_crossentropy', optimizer=keras.optimizers.Adam(),
              metrics=['accuracy'])

	model.load_weights(os.getcwd() + '/ml/model_acc56_2.h5')
	return model

def load_image(image_path):
	image = cv2.imread(image_path)
	image = cv2.resize(image, (img_width, img_height))
	image = image.reshape((1, img_width, img_height, 3))
	return image
